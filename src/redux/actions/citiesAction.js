import { ActionTypes } from '../constants/action-types';

export const getCities = (cities) => {
     return{
         type: ActionTypes.GET_CITIES,
         payload: cities
     }
}